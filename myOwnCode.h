#ifndef MAZE_H
#define MAZE_H

#include "dsets.h"
#include "cs225/PNG.h"
#include <vector>
#include <stdlib.h> 
#include <time.h>
#include <algorithm>
#include <queue>
#include <map>
#include <stack>


using namespace std;
using namespace cs225;
using std::stack;
using std::queue;
using std::map;

class SquareMaze
{
	public:
		SquareMaze(); //constructor
		void makeMaze(int width, int height); //creates a random maze
		bool canTravel(int x, int y, int dir) const; //returns if a wall is in the way or not
		void setWall(int x, int y, int dir, bool exists); //sets walls. used for making maze
		vector<int> solveMaze(); //DFS to solve maze
		PNG* drawMaze() const; //Visualize empty maze
		PNG* drawMazeWithSolution(); //Visualize solved maze
		~SquareMaze();	//deconstructor	
	private:
		class Squares
		{
			public:
			bool right, down;
			Squares() //each square has a right and bottom wall
			{
				right = true; down = true;
			}
			private:
		};
		Squares** squares = NULL;
		DisjointSets* set = NULL;
		int width = 0;
		int height = 0;
		void clear();
		int nextSquare( int x, int y, int dir ); //helper functions
		int returnDir(int idx);
};


#endif