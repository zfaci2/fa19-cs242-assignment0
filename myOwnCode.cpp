#include "maze.h"

SquareMaze::SquareMaze()
{
	//Nothing to do here
}

void SquareMaze::makeMaze(int width, int height)
{
	//clear existing data
	if(this->height != 0 && this->width != 0)
	{
		clear();
	}

	//init squares and set
	this->width = width;
	this->height = height;
	set = new DisjointSets();	
	set->addelements(width*height);
	

	squares = new Squares*[width];	
	for(int i = 0; i < width; i++)
	{
			squares[i]=new Squares[height];
	}
	
	//get rid of random walls
	srand(time(NULL));
	vector<int> random;
	int numOfWalls = width*height*2;
	for(int i = 0; i < numOfWalls; i++)
	{
		random.push_back(i);
	}
	random_shuffle(random.begin(), random.end());
	while(random.size() != 0)
	{
		int wall = random.back();
		random.pop_back();

		int side = 0;
		int x = 0;
		int y = 0;

		if(wall < numOfWalls/2) //right walls
		{
			x = wall % width;
			y = wall / width;
			side = wall + 1;
			if((x + 1 != width) && (set->find(side) != set->find(wall)))
			{
				setWall(x,y,0,false);
				set->setunion(wall, side);
			}
		}
		else //bottom walls
		{
			int newWall = wall - (numOfWalls/2);
			x = newWall % width;
			y = newWall / width;
			side = newWall + width;
			if((y + 1 != height) && (set->find(side) != set->find(newWall)))
			{
				setWall(x,y,1,false);
				set->setunion(newWall, side);
			}
		}
	}
}

bool SquareMaze::canTravel(int x, int y, int dir) const
{
	if(dir == 0)
	{
		if(x >= width || y >= height)
		//out of bounds
		{
			return false;
		}
		else
		{
			return !(squares[x][y].right); //can trvael if no wall
		}
	}
	else if(dir == 1)
	{
		if(x >= width || y >= height)	
		//out of bounds
		{
			return false;
		}
		else
		{
			return !(squares[x][y].down); //can trvael if no wall
		}
	}
	else if(dir == 2)
	{
		if(x < 1)
		//out of bounds
		{
			return false;
		}
		else
		{
			return !(squares[x-1][y].right); //can trvael if no wall
		}
	}
	else //if(dir == 3)
	{
		if(y < 1)
		//out of bounds
		{
			return false;
		}
		else
		{
			return !(squares[x][y-1].down); //can trvael if no wall
		}
	}
}

void SquareMaze::setWall(int x, int y, int dir, bool exists)
{
	if(dir == 0)
	{
		squares[x][y].right = exists;
	}
	else
	{
		squares[x][y].down = exists;		
	}
}

vector<int> SquareMaze::solveMaze()
{
	vector<int> path;

	vector<vector<bool>> visited;
	visited.resize(width);
	for(int i = 0; i < width; i++)
	{
		visited[i].resize(height);
	}
//all this stuff is for the dfs
	queue<int> queue;	
	queue.push(0);
	visited[0][0] = 1;
	map<int,int> map;	
	int longest = 0;
	int init = 0;
	int finish = 0;
	
	//Depth First Search
	while(!queue.empty())
	{
		int square = queue.front();
		queue.pop();

		int x = square % width;
		int y = square / width;
		for(int i = 0; i < 4; i++)
		{
			int square2 = nextSquare(x,y,i);
			int x2 = square2 % width;
			int y2 = square2 / width;

			if(canTravel(x,y,i) && !visited[x2][y2])
			{
				queue.push(square2);				
				map[square2] = square; //map the path of squares			
				visited[x2][y2] = 1;
			}
		}
	}


	//find the biggest distance from the DFS
	for(int i = 0; i < width; i++)
	{
		init = 0;
		int end = i + width*(height - 1); 
		int counter = 0;
		
		while(init != end)
		{
			end = map[end];
			counter++;
		}
		if(counter > longest)
		{
			longest = counter;
			finish = i + width*(height-1);
		}
	}

	stack<int> stack;
	while(finish != 0)
	{
		int idx = finish - map[finish];
		finish = map[finish];

		stack.push(returnDir(idx));
	}
	while(stack.size() != 0)
	//reverse
	{
		path.push_back(stack.top());
		stack.pop();
	}
	return path;
}

PNG* SquareMaze::drawMaze() const
{
	int mazeWidth = width*10+1;
	int mazeHeight = height*10+1;
	PNG* maze = new PNG(mazeWidth, mazeHeight);
	HSLAPixel black(0,0,0);
	//HSLAPixel* temp = maze->getPixel(0,0);
	//*temp = black;
	
	//blacken bottom and side
	for(int i = 10; i < mazeWidth; i++)
	{
		*(maze->getPixel(i,0)) = black;
		//*temp = black;
	}
	for(int i = 0; i < mazeHeight; i++)
	{
		*(maze->getPixel(0,i)) = black; 
		//*temp = black;
	}

	for(int i = 0; i < width; i++)
	{
		for(int j = 0; j < height; j++)
		{
			if(!canTravel(i, j, 1) && !canTravel(i, j + 1, 3))
			{
				for(int k = 0; k <= 10; k++)
					*(maze->getPixel(i * 10 + k,(j + 1)*10)) = black;
					//*temp = black;
			}

			if(!canTravel(i, j, 0) && !canTravel(i + 1, j, 2))
			{
				for(int k = 0; k <= 10; k++)
					*(maze->getPixel((i + 1)*10, j*10 + k)) = black;
					//*temp = black;
			}
		}
	}
	
	return maze;
}


PNG* SquareMaze::drawMazeWithSolution()
{
	PNG* maze = drawMaze();
	vector<int> track = solveMaze();
	
	HSLAPixel red(0, 1, 0.5, 1);
	HSLAPixel* temp;
	
	int x = 5; //start in middle of square
	int y = 5;

	for(unsigned long i = 0; i < track.size(); i++)
	{
		if(track[i] == 0)
		{
			for(int n = 0; n < 11; n++)
			{
				temp = maze->getPixel(x+n,y);
				*temp = red;
			}
			x += 10; //go right
		}
		else if(track[i] == 1)
		{
			for(int n = 0; n < 11; n++)
			{
				temp = maze->getPixel(x,y+n);
				*temp = red;
			}
			y += 10; //go down
		}
		else if(track[i] == 2)
		{
			for(int n = 0; n < 11; n++)
			{
				temp = maze->getPixel(x-n,y);
				*temp = red;
			}
			x -= 10; //gp left
		}
		else
		{
			for(int n = 0; n < 11; n++)
			{
				temp = maze->getPixel(x,y-n);
				*temp = red;
			}
			y -= 10; //go up
		}
	}


	HSLAPixel white(0, 0, 1);
	
	for(int n = 0; n < 9; n++)
	{
		temp = maze->getPixel((x-5) + n + 1, (y+5));
		*temp = white;
		//whiten exit
	}

	return maze;
}

SquareMaze::~SquareMaze()
{
	clear();
}

void SquareMaze::clear()
{
	delete set;	

	for(int i = 0; i < width; i++)
	{
		delete[] squares[i];
	}
	delete[] squares;
}

int SquareMaze::returnDir(int idx)
{
	if(idx == 1)
	{
		return 0;
	}
	else if(idx == width)
	{
		return 1;
	}
	else if(idx == -1)
	{
		return 2;
	}
	else
	{
		return 3;
	}
}

int SquareMaze::nextSquare( int x, int y, int dir )
{

	int temp = 0;

	if(dir == 0)
	{
		temp = 1;
	}
	else if(dir == 1)
	{
		temp = width;
	}
	else if(dir == 2)
	{
		temp = -1;
	}
	else
	{
		temp = -1 * width;
	}

	return x + temp + y * width;
}